import json
import requests
import random
from secrets import token
from datetime import date


class Playlist:
    def __init__(self, id, name, length):
        self.id = id
        self.name = name
        self.length = length


class SaveSongs:
    playlists = []
    selected_playlist_id = ''
    songs = []
    length = 0

    def __init__(self):
        self.spotify_token = token

    def get_user_playlists(self):
        # Loop through playlist tracks, add them to list
        query = "https://api.spotify.com/v1/me/playlists"
        response = requests.get(query,
                                headers={"Content-Type": "application/json",
                                         "Authorization": "Bearer {}".format(self.spotify_token)})

        response_json = response.json()

        for i in response_json["items"]:
            playlist_temp = Playlist(i["id"], i["name"], i["tracks"]["total"])
            self.playlists.append(playlist_temp)
        self.select_playlist()

    def select_playlist(self):
        print("Select the playlist you want to mix up:")
        for i in range(len(a.playlists)):
            print(str(i) + " " + a.playlists[i].name)

        number = int(input())

        self.selected_playlist_id = self.playlists[number].id
        self.length = self.playlists[number].length
        self.reorder_playlist()

    def reorder_playlist(self):
        # Create a new playlist
        print("Reordering playlists....")
        for x in range(self.length):
            start_rnd = x
            insert_before_rnd = random.randrange(0, self.length)
            self.reorder_track(start_rnd, insert_before_rnd)

    def reorder_track(self, start, insert_before):
        query = "https://api.spotify.com/v1/playlists/{}/tracks".format(self.selected_playlist_id)

        request_body = json.dumps({
            "range_start": start,
            "insert_before": insert_before,
            "range_length": 1
        })

        response = requests.put(query, data=request_body, headers={
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(self.spotify_token)
        })

# Initialize class
a = SaveSongs()
# Logiv
a.get_user_playlists()
